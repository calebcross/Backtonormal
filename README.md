# Back to Normal
![Screenshot of Back To Normal](https://user-images.githubusercontent.com/4029060/118316928-691d0a80-b4c5-11eb-8049-c2d3faf77531.png)

Back to Normal started as a individual 2-week Capstone project for BrainStation Web Development Bootcamp in Miami, Florida

It’s a react app built with a few different  libraries that I list below  partnered with a GraphQL server built with Node and Express.
## Front End

### Libraries

## Back End
[GraphiQL Interface](https://backtonormal.herokuapp.com/graphql)

### Libraries


## Author
[GitHub](https://github.com/calebcross)
[LinkedIn](https://www.linkedin.com/in/calebacross/)

## Credits
[Font Awesome 5](https://fontawesome.com/start)


## License
[MIT](https://choosealicense.com/licenses/mit/)
